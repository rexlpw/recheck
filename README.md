# This is RExCHECKin Project

----
## usage

開啟UART及I2C功能

    raspi-config

正常來說樹莓派在安裝好之後，預設的瀏覽器就是 Chromium，不需要另外安裝，如果有例外的狀況需要自行安裝的話，可用 apt 安裝：

    # 安裝 Chromium 瀏覽器
    sudo apt-get install chromium-browser

安裝 Python 的 Selenium 模組：

    # 安裝 Selenium 模組
    pip3 install selenium

從 [launchpad.net](https://launchpad.net/ubuntu/trusty/+package/chromium-chromedriver) 下載 Chromium 瀏覽器專用的 WebDriver，由於樹莓派是用 ARM 的 CPU，下載時要選擇 armhf (Updates) 的版本。

    # 下載 Chromium 瀏覽器 WebDriver
    wget http://launchpadlibrarian.net/361669488/chromium-chromedriver_65.0.3325.181-0ubuntu0.14.04.1_armhf.deb

下載之後，接著安裝：

    # 安裝 Chromium 瀏覽器 WebDriver
    sudo dpkg -i chromium-chromedriver_65.0.3325.181-0ubuntu0.14.04.1_armhf.deb

安裝PyVirtualDisplay

    pip3 install pyvirtualdisplay
    sudo apt-get install xvfb

安裝SMBUS(1602 LCD使用)

    pip install smbus
    
查詢1602 LCD所佔用的I2C位址

    i2cdetect -y 1
    
修改程式內1602 LCD的位址

    LCD1602.init(<修改位址>, boolean)

開機自動打開終端機並執行本程式(有螢幕的情況下)

    # 編寫指令並儲存成/home/pi/autostart.sh
    nano /home/pi/autostart.sh
    ==below==
    #!/bin/bash

    cd /home/pi/recheck
    python3 readcard.py
    ==above==
    
    # 給予執行權限
    chmod +x /home/pi/autostart.sh
    
    # 編輯lxsession檔案(若無資料夾可自建)
    nano /home/pi/.config/lxsession/LXDE-pi/autostart
    ==below==
    lxterminal --command="/home/pi/autostart.sh"
    ==above==

開機自動執行本程式(僅配置其他顯示器的情況下)

    # 編輯rc.local
    sudo nano /etc/rc.local
    # 在exit 0前貼上下面這列
    # autostart.sh內容如上
    su pi -c "exec /home/pi/autostart.sh"

每晚自動重新開機

    # EDIT crontab
    sudo crontab -e
    # auto reboot on midnight
    @midnight /sbin/shutdown -r now
    # auto reboot on AM04:00
    0 4 * * * /sbin/shutdown -r now

----
## changelog
* 2020/12/18 add systemd/service support
* 2020/05/15 add e-paper support
* 2020/02/10 add 1602LCD support
* 2020/02/07 update README.md
* 2019/12/02 stable init

----
## ref
* [PN532 sample code](https://www.waveshare.com/w/upload/6/67/Pn532-nfc-hat-code.7z)
* [waveshare pn532 doc](http://www.waveshare.net/wiki/PN532_NFC_HAT)
* [waveshare pn532 doc(EN)](https://www.waveshare.com/wiki/PN532_NFC_HAT#Demo_codes)
* [RPi autostart terminal & autostart](https://ssk7833.github.io/blog/2018/12/10/raspberry-pi-autostart-lxterminal-and-run-python/)
* [RPi crontab reboot](https://www.raspberrypi.org/forums/viewtopic.php?t=118069)
* [1602 LCD module](https://atceiling.blogspot.com/2019/10/raspberry-pi-53i2clcd1620.html)
* [Linux Systemd 設定開機自動啟動的程式、服務](https://blog.longwin.com.tw/2020/05/linux-systemd-boot-start-service-2020/)
