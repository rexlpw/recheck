'''
origin coding by lian
'''

import os
import json
import time
import random
import smtplib
import argparse
import datetime
import requests

latitude = 25.077350428746257
longitude = 121.57503086946213

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
url = "https://pro.104.com.tw/hrm/psc/apis/public/punchCard.action"
headers = {
    "User-Agent": "104%E4%BC%81%E6%A5%AD%E5%A4%A7%E5%B8%AB/201020003 CFNetwork/1197 Darwin/20.0.0"
}


def do_checkin(name, cid, pid, deviceId):
    with open(os.path.join(BASE_DIR, "cookies.json")) as f:
        cookie_data = json.loads(f.read())

    checkin_result = 3
    error_code = 0

    print("Hi {0}，正在嘗試幫您打卡中，請稍後......\n({1})".format(name, datetime.now()))
    try:
        for _ in range(5):
            time.sleep(random.random() + 0.1)
            with requests.session() as sess:
                sess.headers.update(headers)
                for k in cookie_data:
                    sess.cookies[k] = cookie_data[k]
                    print(k)
                    print(cookie_data[k])
                try:
                    payload = []
                    payload["cid"] = cid
                    payload["pid"] = pid
                    payload["deviceId"] = deviceId
                    payload["latitude"] = latitude
                    payload["longitude"] = longitude
                    payload["latitude"] += (random.random() - random.random()) / 1000
                    payload["longitude"] += (random.random() - random.random()) / 1000
                    resp = sess.post(url, json=payload)
                    resp_data = json.loads(resp.text)
                    if not resp_data["success"]:
                        raise Exception(resp.text)
                except:
                    print(resp)
                    print(resp.headers)
                    print(resp.text)
                else:
                    print("Done.")

            checkin_result = 1
    except Exception as e:
        print("failed by: {0}".format(e))
        error_code += 1
        checkin_result = 3
    finally:
        time.sleep(3)
    return checkin_result
