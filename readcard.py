import RPi.GPIO as GPIO
import time
import json
import checkin
from datetime import datetime
import LCD1602
import speaker
from pn532 import *
import relay

CONST_SPEAKER_BCM_PINOUT = 13  # speaker pinout
CONST_RELAY_BCM_PINOUT = 18  # relay module pinout
CONST_LCD1602_ADDRESS = 0x27  # LCD1602 device address

user_data = None  # user data


def get_email_pwd(cardid):
    for user in user_data:
        for card in user["cards"]:
            if card == cardid:  # valid card holder found
                return 1, user['name'], user["cid"], user["pid"], user["deviceId"]
    return 2, '', '', '', ''  # not found


if __name__ == '__main__':
    try:
        GPIO.setmode(GPIO.BCM)
        # load user_data
        user_data_file = "user_data.json"
        with open(user_data_file) as f:
            user_data = json.loads(f.read())
            if not isinstance(user_data, list) or not user_data:
                raise AssertionError('User data should be in a list and cannot be empty')
            for item in user_data:
                if "name" not in item or "cid" not in item or "pid" not in item or "deviceId" not in item:
                    raise AssertionError('user_data.json format error, please check')

        # please check address before use
        LCD1602.init(CONST_LCD1602_ADDRESS, 1)
        LCD1602.clear()
        LCD1602.write(0, 0, 'Hi! I\'m Rex')
        LCD1602.write(2, 1, 'WELCOME')
        # loop for wait valid card
        while True:
            time.sleep(3)  # 休息3秒
            cardid = None

            # Work with UART mode
            pn532 = PN532_UART(debug=False, reset=20)

            # Configure PN532 to communicate with MiFare cards
            pn532.SAM_configuration()

            print('\n\n請感應您的識別證，感應區在機器後方')
            LCD1602.clear()
            LCD1602.write(0, 0, 'Please TOUCH')
            LCD1602.write(2, 1, 'YOUR ID CARD.')
            # loop for read card
            while True:
                try:
                    # Check if a card is available to read
                    uid = pn532.read_passive_target(timeout=0.5)
                    # Try again if no card is available.
                    if uid is None:
                        continue
                    else:
                        cardid = ''.join(['{:02x}'.format(i) for i in uid]).upper()
                        LCD1602.clear()
                        LCD1602.write(0, 0, 'Card ID:' + cardid)
                        LCD1602.write(2, 1, 'Checking......')
                        break
                except Exception as e:
                    pass

            checkin_result, name, cid, pid, deviceId = get_email_pwd(cardid)
            if checkin_result == 1:
                checkin_result = checkin.do_checkin(name, cid, pid, deviceId)
            if checkin_result == 1:
                print("{0}({1})打卡成功({2})".format(name, cardid, datetime.now()))
                LCD1602.clear()
                LCD1602.write(0, 0, 'Card ID:' + cardid)
                LCD1602.write(1, 1, 'Check SUCCESS!')
                relay.relay_on(CONST_RELAY_BCM_PINOUT)  # fly~
            elif checkin_result == 2:
                print("您的卡片ID:{0}尚未留存帳密，無法打卡".format(cardid))
                LCD1602.clear()
                LCD1602.write(0, 0, 'No ID&PW')
                LCD1602.write(2, 1, 'FOUND!')
            elif checkin_result == 3:
                print("Hi {0}，您的卡片ID:{1}打卡失敗，請協助修bug".format(name, cardid))
                LCD1602.clear()
                LCD1602.write(0, 0, 'You\'ve some')
                LCD1602.write(0, 1, 'error occurred.')
            # speaker.beep(CONST_SPEAKER_BCM_PINOUT, checkin_result)
            time.sleep(3)

    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()
        LCD1602.clear()
