import RPi.GPIO as GPIO
import time


# 1:success 2:ID not in json 3:checkin failure
def beep(CONST_SPEAKER_BCM_PINOUT, beep_mode):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(CONST_SPEAKER_BCM_PINOUT, GPIO.OUT)
    p = GPIO.PWM(CONST_SPEAKER_BCM_PINOUT, 50)
    p.start(50)

    for i in range(0, beep_mode):
        p.ChangeFrequency(659)
        time.sleep(0.5)
        if beep_mode > 1:
            time.sleep(0.3)

    p.stop()
    GPIO.cleanup()
