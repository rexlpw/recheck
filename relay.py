import RPi.GPIO as GPIO
import time


def relay_on(CONST_RELAY_BCM_PINOUT):
    try:
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(CONST_RELAY_BCM_PINOUT, GPIO.OUT, initial=GPIO.HIGH)
        GPIO.output(CONST_RELAY_BCM_PINOUT, GPIO.LOW)
        time.sleep(2)
        GPIO.output(CONST_RELAY_BCM_PINOUT, GPIO.HIGH)
    finally:
        GPIO.cleanup()
